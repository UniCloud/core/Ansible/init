# Different OS's differ on `which bash` So we're not using a Shebang. Keep .sh extension instead!

detectEnv(){
  # prime. /unix.stackexchange.com/questions/6345/how-can-i-get-distribution-name-and-version-number-in-a-simple-shell-script

  if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
  elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
  elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
  elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
  elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    ...
  elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    ...
  else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
  fi
}


CentOS(){
  # https://fedoraproject.org/wiki/EPEL
  # https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-rhel-centos-or-fedora

  sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
  sudo dnf config-manager --set-enabled PowerTools
  sudo dnf install ansible -y
}

Ubuntu(){
  # doc: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu
  sudo apt install software-properties-common
  if [ "$VER" != "20.04" ];then
  # Skip if 20:04 as PPA's not updated yet
    echo "Using PPA ..."
    sudo apt-add-repository --yes --update ppa:ansible/ansible
    sudo apt update
  fi
  sudo apt install ansible -y
}

Debian(){
  echo
}

main(){
  detectEnv
  if ! which ansible >/dev/null; then
    echo "installing Ansible on $OS $VER"

    if [ "$OS" = "CentOS Linux" ]; then
      CentOS
    elif [ "$OS" = "Ubuntu" ]; then
      Ubuntu
    elif [ "$OS" = "Debian" ]; then
      Debian
    else
      echo "other unknown distribution"
    fi
  fi

  path=`which ansible`
  echo "installed at $path"

  ansible --version
}

main
exit


### Todo:
Everything_in_sudo(){
  # https://www.runescape.com/download#linux-instructions
  echo $VER
  sudo -s -- << EOF
    echo hello
EOF
}
