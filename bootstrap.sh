pre(){ # conditions
# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
  ownpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
  cd $ownpath
}

installer(){
  # echo 'Start by installing Ansible!'
  ./ansible/install.sh
  # echo 'now install Salt locally'
}

simpleHostVars(){
  echo 'make it work without Inventory'
  echo "ansible_become_password: $password
  ansible_user: $USER" > ansible/SaltStack-local/vars/main.yml
}

askPassword(){
  echo -n Sudo Password:
  read -s password
  echo
  echo $password
  simpleHostVars
}

main(){
  #askPassword
  pre
  installer
  #exec ansible-playbook saltstack/inst-local.yml
}

main
