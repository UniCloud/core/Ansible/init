# bootstrap

- Install UniAI, Cloud, & Env on any base distro!
- then pull in your settings!

## Run
All in one.
```
uniroot=~/.uni
mkdir $uniroot ; cd $uniroot ; wget -c https://gitlab.com/UniCloud/bootstrap/-/archive/master/bootstrap-master.tar.gz ; tar xfz bootstrap-master.tar.gz ; ./bootstrap-master/bootstrap.sh
```

Or: if you have Git.
```
uniroot=~/.uni
mkdir $uniroot ; cd $uniroot ; git clone --depth=1 https://gitlab.com/UniCloud/bootstrap.git ; ./bootstrap/bootstrap.sh
```
